package com.crol.model;

public class TipoOperacion {
	private Integer idTipoOperacion;
	private String nombre;
	public Integer getIdTipoOperacion() {
		return idTipoOperacion;
	}
	public void setIdTipoOperacion(Integer idTipoOperacion) {
		this.idTipoOperacion = idTipoOperacion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}
