package com.crol.controllers;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.persistence.oxm.JSONWithPadding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.codehaus.jettison.json.JSONException;
//import org.codehaus.jettison.json.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.crol.model.MovimientoResponse;
import com.crol.model.MovimientoVO;
import com.crol.model.SocioVO;
import com.crol.model.loginResponse;
import com.crol.util.Constantes;
import com.crol.util.Utilitario;
import com.google.gson.Gson;



@RestController
public class LoginController {

	@Value("${config.ruta-datos.url}")
	private String rutaConfigDatos;
	
	@PostMapping("/login")
	public SocioVO obtenerAhorro(@RequestBody SocioVO socio){
		
		SocioVO loginR=new SocioVO();
		try {
			RestTemplate restTemplate=new RestTemplate();
			loginR=restTemplate.postForObject(rutaConfigDatos+"/login",socio, SocioVO.class); 
			loginR.setSuccess(true);
		}catch(Exception ex) {
			loginR.setSuccess(false);
			loginR.setMensaje("Ocurrió un problema al obtener: "+ ex.getMessage());
			
		}
		return loginR;
	}
	
	
	
	private List<HttpMessageConverter<?>> getMessageConverters() {
	    List<HttpMessageConverter<?>> converters = 
	      new ArrayList<HttpMessageConverter<?>>();
	    converters.add(new MappingJackson2HttpMessageConverter());
	    return converters;
	}
}
