package com.crol.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.crol.model.SocioVO;
import com.crol.model.TransferenciaVO;
import com.crol.util.Constantes;



@RestController
public class AportesController {

	@Value("${config.ruta-datos.url}")
	private String rutaConfigDatos;
	
	@PostMapping("/deudaAportes")
	public Map<String, Object> obtenerDeuda(@RequestBody SocioVO socio) throws Exception{
		Map<String, Object> fpsResponse=new HashMap<>();
		try {
			RestTemplate restTemplate=new RestTemplate();
			fpsResponse=restTemplate.postForObject(rutaConfigDatos+"/deudaAportes", socio,HashMap.class);
			return fpsResponse;
		}catch(Exception e) {
			fpsResponse.put("success", false);
			fpsResponse.put("mensaje", e.getMessage());
			return fpsResponse;
		}
	}
	@PostMapping("/pagoAportes")
	public TransferenciaVO transferenciaPropias(@RequestBody TransferenciaVO transferencia ){
		TransferenciaVO transferenciaR=new TransferenciaVO();
		try {
			RestTemplate restTemplate=new RestTemplate();
			transferenciaR=restTemplate.postForObject(rutaConfigDatos+"/pagoAportes",transferencia, TransferenciaVO.class); 
			transferenciaR.setSuccess(transferenciaR.isSuccess());
		}catch(Exception ex) {
			transferenciaR.setSuccess(false);
			transferenciaR.setDetalleRespuesta("Ocurrió un problema al obtener: "+ ex.getMessage());
		}
		return transferenciaR;
	}
}
