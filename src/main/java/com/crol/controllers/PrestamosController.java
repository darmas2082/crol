package com.crol.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.crol.model.PinResponse;
import com.crol.model.SocioVO;
import com.crol.model.TransferenciaVO;
import com.crol.model.listaPrestamosResponse;
import com.crol.util.Constantes;
import com.crol.model.PrestamosVO;

@RestController
public class PrestamosController {

	@Value("${config.ruta-datos.url}")
	private String rutaConfigDatos;
	
	@PostMapping("/prestamos")
	public listaPrestamosResponse validaPin(@RequestBody SocioVO socioVO){
		
		listaPrestamosResponse listaPrestamosR=new listaPrestamosResponse();
		try {
			RestTemplate restTemplate=new RestTemplate();
			listaPrestamosR=restTemplate.postForObject(rutaConfigDatos+"/prestamos",socioVO, listaPrestamosResponse.class); 
			listaPrestamosR.setSuccess(listaPrestamosR.isSuccess());
		}catch(Exception ex) {
			listaPrestamosR.setSuccess(false);
			listaPrestamosR.setMensaje("Ocurrió un problema al obtener: "+ ex.getMessage());
			
		}
		return listaPrestamosR;
	}
	
	@PostMapping("/movimientoPrestamo")
	public PrestamosVO validaPin(@RequestBody PrestamosVO prestamoVO){
		
		PrestamosVO listaPrestamosR=new PrestamosVO();
		try {
			RestTemplate restTemplate=new RestTemplate();
			listaPrestamosR=restTemplate.postForObject(rutaConfigDatos+"/movimientoPrestamo",prestamoVO, PrestamosVO.class); 
			listaPrestamosR.setSuccess(listaPrestamosR.getSuccess());
		}catch(Exception ex) {
			listaPrestamosR.setSuccess(false);
			listaPrestamosR.setMensaje("Ocurrió un problema al obtener: "+ ex.getMessage());
			
		}
		return listaPrestamosR;
	}
	
	@PostMapping("/prestamoPendiente")
	public listaPrestamosResponse obtenerPrestamosPendiente(@RequestBody PrestamosVO prestamosVO) {
		listaPrestamosResponse listaPrestamosR=new listaPrestamosResponse();
		try {
			RestTemplate restTemplate=new RestTemplate();
			listaPrestamosR=restTemplate.postForObject(rutaConfigDatos+"/prestamoPendiente",prestamosVO, listaPrestamosResponse.class); 
			listaPrestamosR.setSuccess(listaPrestamosR.isSuccess());
		}catch(Exception ex) {
			listaPrestamosR.setSuccess(false);
			listaPrestamosR.setMensaje("Ocurrió un problema al obtener: "+ ex.getMessage());
			
		}
		return listaPrestamosR;
	}
	
	@PostMapping("/pagoPrestamo")
	public TransferenciaVO transferenciaPropias(@RequestBody TransferenciaVO transferencia ){
		TransferenciaVO transferenciaR=new TransferenciaVO();
		try {
			RestTemplate restTemplate=new RestTemplate();
			transferenciaR=restTemplate.postForObject(rutaConfigDatos+"/pagoPrestamo",transferencia, TransferenciaVO.class); 
			transferenciaR.setSuccess(transferenciaR.isSuccess());
		}catch(Exception ex) {
			transferenciaR.setSuccess(false);
			transferenciaR.setDetalleRespuesta("Ocurrió un problema al obtener: "+ ex.getMessage());
		}
		return transferenciaR;
	}
}
