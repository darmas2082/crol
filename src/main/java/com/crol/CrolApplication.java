package com.crol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrolApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrolApplication.class, args);
	}

}
