package com.crol.model;

public class Ahorros {

	private String IdAhorro;
	private String Moneda;
	private String Descripcion;
	private String Saldo;
	private String tipoCuenta;
	private String nroCuenta;
	
	private boolean esOrigen;
	private double saldoDiarioTransferencia;
	private boolean esDestino;
	
	
	
	public boolean isEsOrigen() {
		return esOrigen;
	}
	public void setEsOrigen(boolean esOrigen) {
		this.esOrigen = esOrigen;
	}
	public double getSaldoDiarioTransferencia() {
		return saldoDiarioTransferencia;
	}
	public void setSaldoDiarioTransferencia(double saldoDiarioTransferencia) {
		this.saldoDiarioTransferencia = saldoDiarioTransferencia;
	}
	public boolean isEsDestino() {
		return esDestino;
	}
	public void setEsDestino(boolean esDestino) {
		this.esDestino = esDestino;
	}
	public String getTipoCuenta() {
		return tipoCuenta;
	}
	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}
	public String getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(String nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public String getIdAhorro() {
		return IdAhorro;
	}
	public void setIdAhorro(String idAhorro) {
		IdAhorro = idAhorro;
	}
	public String getMoneda() {
		return Moneda;
	}
	public void setMoneda(String moneda) {
		Moneda = moneda;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	public String getSaldo() {
		return Saldo;
	}
	public void setSaldo(String saldo) {
		Saldo = saldo;
	}
	
}
