package com.crol.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Utilitario {
	public static String obtenerJson(String urlWS) {

        System.out.println("*****URL[" + urlWS + "]");

        String output = "";
        HttpURLConnection conn = null;
        try {

            URL url = new URL(urlWS);//your url i.e fetch data from .
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/x-javascript");
            //conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            if ((output = br.readLine()) != null) {
                System.out.println("Hilo: [" + Thread.currentThread() + "] :[" + output + "]");
            }

        } catch (Exception e) {
            System.out.println("Exception in NetClientGet:- " + e);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
            return output;
        }
    }
}
