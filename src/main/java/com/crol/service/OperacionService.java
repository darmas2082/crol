package com.crol.service;

import com.crol.model.Operacion;

public interface OperacionService {

	public Operacion EjecutaOperacion(Operacion operacion);
}
