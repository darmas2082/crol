package com.crol.model;

public class CampoTrama {

	private boolean variable;
	private String campo;
	private Integer longitudMaxima;
	private String valor;
	private String codigoCampo;
	
	public String getCodigoCampo() {
		return codigoCampo;
	}
	public void setCodigoCampo(String codigoCampo) {
		this.codigoCampo = codigoCampo;
	}
	public boolean isVariable() {
		return variable;
	}
	public void setVariable(boolean variable) {
		this.variable = variable;
	}
	public String getCampo() {
		return campo;
	}
	public void setCampo(String campo) {
		this.campo = campo;
	}
	public Integer getLongitudMaxima() {
		return longitudMaxima;
	}
	public void setLongitudMaxima(Integer longitudMaxima) {
		this.longitudMaxima = longitudMaxima;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
}
