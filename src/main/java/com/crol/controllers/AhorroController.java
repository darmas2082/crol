package com.crol.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.crol.model.listaAhorrosResponse;
import com.crol.model.loginResponse;
import com.crol.util.Constantes;
import com.crol.util.Utilitario;
import com.google.gson.Gson;

@RestController
public class AhorroController {

	@Value("${config.ruta-datos.url}")
	private String rutaConfigDatos;
	
	@PostMapping("/ahorros")
	public listaAhorrosResponse obtenerAhorroPost(@RequestBody listaAhorrosResponse ahorro){
		
		
		listaAhorrosResponse listaAhorrosR=new listaAhorrosResponse();
		try {
			
			RestTemplate restTemplate=new RestTemplate();
			listaAhorrosR=restTemplate.postForObject(rutaConfigDatos+"/ahorros", ahorro,listaAhorrosResponse.class);
			listaAhorrosR.setSuccess(true);
			
		}catch(Exception ex) {
			listaAhorrosR.setSuccess(false);
			listaAhorrosR.setMensaje(ex.getMessage());
			
		}
		return listaAhorrosR;
	}
}
