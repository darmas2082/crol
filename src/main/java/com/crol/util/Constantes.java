package com.crol.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

@Repository
public class Constantes {

	@Value("${config.ruta-datos.url}")
	private String rutaConfigDatos;
	
	public  String rutaDatos=rutaConfigDatos;
}
