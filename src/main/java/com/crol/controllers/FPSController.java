package com.crol.controllers;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.persistence.oxm.JSONWithPadding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.crol.model.Prestamos;
import com.crol.model.SocioVO;
import com.crol.model.TransferenciaVO;
import com.crol.model.listaAhorrosResponse;
import com.crol.model.listaFPSResponse;
import com.crol.model.loginResponse;
import com.crol.util.Constantes;
import com.crol.util.Utilitario;
import com.google.gson.Gson;


@RestController
public class FPSController {

	@Value("${config.ruta-datos.url}")
	private String rutaConfigDatos;
	
	@PostMapping("/listarfps")
	public listaFPSResponse obtenerAhorroPost(@RequestBody Prestamos prestamos){
		
		
		listaFPSResponse listaAhorrosR=new listaFPSResponse();
		try {
			
			RestTemplate restTemplate=new RestTemplate();
			listaAhorrosR=restTemplate.postForObject(rutaConfigDatos+"/listarfps", prestamos,listaFPSResponse.class);
			listaAhorrosR.setSuccess(listaAhorrosR.getSuccess());
			
		}catch(Exception ex) {
			listaAhorrosR.setSuccess(false);
			listaAhorrosR.setMensaje(ex.getMessage());
			
		}
		return listaAhorrosR;
	}
	
	@PostMapping("/deudaFPS")
	public Map<String, Object> obtenerDeuda(@RequestBody SocioVO socio) throws Exception{
		Map<String, Object> fpsResponse=new HashMap<>();
		try {
			RestTemplate restTemplate=new RestTemplate();
			fpsResponse=restTemplate.postForObject(rutaConfigDatos+"/deudaFPS", socio,HashMap.class);
			return fpsResponse;
		}catch(Exception e) {
			fpsResponse.put("success", false);
			fpsResponse.put("mensaje", e.getMessage());
			return fpsResponse;
		}
	}
	
	@PostMapping("/pagoFPS")
	public TransferenciaVO transferenciaPropias(@RequestBody TransferenciaVO transferencia ){
		TransferenciaVO transferenciaR=new TransferenciaVO();
		try {
			RestTemplate restTemplate=new RestTemplate();
			transferenciaR=restTemplate.postForObject(rutaConfigDatos+"/pagoFPS",transferencia, TransferenciaVO.class); 
			transferenciaR.setSuccess(transferenciaR.isSuccess());
		}catch(Exception ex) {
			transferenciaR.setSuccess(false);
			transferenciaR.setDetalleRespuesta("Ocurrió un problema al obtener: "+ ex.getMessage());
		}
		return transferenciaR;
	}
}
