package com.crol.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.crol.model.MovimientoResponse;
import com.crol.model.MovimientoVO;
import com.crol.model.PinResponse;
import com.crol.model.SocioVO;
import com.crol.util.Constantes;

@RestController
public class PinController {
	
	@Value("${config.ruta-datos.url}")
	private String rutaConfigDatos;

	@PostMapping("/validaPin")
	public PinResponse validaPin(@RequestBody SocioVO socioVO){
		
		PinResponse pinResponse=new PinResponse();
		try {
			RestTemplate restTemplate=new RestTemplate();
			pinResponse=restTemplate.postForObject(rutaConfigDatos+"/validaPin",socioVO, PinResponse.class); 
			pinResponse.setSuccess(pinResponse.getSuccess());
		}catch(Exception ex) {
			pinResponse.setSuccess(false);
			pinResponse.setMensaje("Ocurrió un problema al obtener: "+ ex.getMessage());
			
		}
		return pinResponse;
	}
	
	@PostMapping("/creaPin")
	public PinResponse crearPin(@RequestBody SocioVO socioVO){
		
		PinResponse pinResponse=new PinResponse();
		try {
			RestTemplate restTemplate=new RestTemplate();
			pinResponse=restTemplate.postForObject(rutaConfigDatos+"/creaPin",socioVO, PinResponse.class); 
			pinResponse.setSuccess(pinResponse.getSuccess());
		}catch(Exception ex) {
			pinResponse.setSuccess(false);
			pinResponse.setMensaje("Ocurrió un problema al obtener: "+ ex.getMessage());
			
		}
		return pinResponse;
	}
}
