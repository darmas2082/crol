package com.crol.model;

import java.util.List;

public class Operacion {
	private String trama;
	private String canal;
	private List<CampoTrama> listaCampoTrama;
	
	
	
	public List<CampoTrama> getListaCampoTrama() {
		return listaCampoTrama;
	}
	public void setListaCampoTrama(List<CampoTrama> listaCampoTrama) {
		this.listaCampoTrama = listaCampoTrama;
	}
	public String getTrama() {
		return trama;
	}
	public void setTrama(String trama) {
		this.trama = trama;
	}
	public String getCanal() {
		return canal;
	}
	public void setCanal(String canal) {
		this.canal = canal;
	}

}
