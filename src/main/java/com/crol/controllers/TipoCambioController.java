package com.crol.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.crol.model.TipoCambio;
import com.crol.model.listaAhorrosResponse;
import com.crol.util.Constantes;



@RestController
public class TipoCambioController {

	@Value("${config.ruta-datos.url}")
	private String rutaConfigDatos;
	
	@PostMapping("/tipoCambio")
	public TipoCambio obtenerTipoCambio(@RequestBody TipoCambio tipoCambio ){
		
		try {
			
			RestTemplate restTemplate=new RestTemplate();
			tipoCambio=restTemplate.postForObject(rutaConfigDatos+"/tipoCambio", tipoCambio,TipoCambio.class);
			tipoCambio.setSuccess(tipoCambio.isSuccess());
			
		}catch(Exception ex) {
			tipoCambio.setSuccess(false);
			tipoCambio.setMensaje(ex.getMessage());
			
		}
		return tipoCambio;
	}
	
}
