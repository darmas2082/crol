package com.crol.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.crol.model.MovimientoResponse;
import com.crol.model.MovimientoVO;
import com.crol.util.Constantes;

@RestController
public class MovimientoAhorroController {
	
	@Value("${config.ruta-datos.url}")
	private String rutaConfigDatos;

	@PostMapping("/movimientosahorros")
	public MovimientoResponse obtenerAhorro(@RequestBody MovimientoVO movimiento){
		
		MovimientoResponse movimientosR=new MovimientoResponse();
		try {
			RestTemplate restTemplate=new RestTemplate();
			movimientosR=restTemplate.postForObject(rutaConfigDatos+"/movimientosahorros",movimiento, MovimientoResponse.class); 
			movimientosR.setSuccess(true);
		}catch(Exception ex) {
			movimientosR.setSuccess(false);
			movimientosR.setMensaje("Ocurrió un problema al obtener: "+ ex.getMessage());
			
		}
		return movimientosR;
	}
}
